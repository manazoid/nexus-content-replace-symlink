@echo run
setlocal enableextensions enabledelayedexpansion

:: set str=paths.txt


set "file=%~dp0paths.txt"
set /A i=0

for /F "usebackq delims=" %%a in ("%file%") do (
set /A i+=1
call set array[%%i%%]=%%a
call set n=%%i%%
)

for /L %%i in (1,1,%n%) do (
    set str=%%array[%%i]%%
    echo %str%

    :: find index BEGIN

    SET "substr=Nexus Content"
    SET stemp=%str%&SET pos=0
    :loop
        SET /a pos+=1
        echo %stemp%|FINDSTR /b /c:"%substr%" >NUL
        IF ERRORLEVEL 1 (
            SET stemp=%stemp:~1%
            IF DEFINED stemp GOTO loop
            SET pos=0
        )

    :: find index END

    call :strlen result str
    set /a pos-=1
    set /a c=%result%-%pos%
    CALL SET _substring=%%str:~%pos%,%c%%%
    echo %_substring%
    pause
)

REM ********* function *****************************
:strlen <resultVar> <stringVar>
(
    setlocal EnableDelayedExpansion
    (set^ tmp=!%~2!)
    if defined tmp (
        set "len=1"
        for %%P in (4096 2048 1024 512 256 128 64 32 16 8 4 2 1) do (
            if "!tmp:~%%P,1!" NEQ "" (
                set /a "len+=%%P"
                set "tmp=!tmp:~%%P!"
            )
        )
    ) ELSE (
        set len=0
    )
)
(
    endlocal
    set "%~1=%len%"
    exit /b
)
