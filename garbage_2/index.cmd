@echo OFF

setlocal

Set "str1=This is Test string"
Set "sstr=is"

call :substringIndex result str1 sstr

echo %result%
ECHO Pos of "%sstr%" IN "%str1%" = %result
pause


:substringIndex <resultVar> <stringVar> <findingString>
(
    setlocal EnableDelayedExpansion
    (set^ tmp=!%~2!)
    if defined tmp (
        :loop
        SET /a pos+=1
        echo %stemp%|FINDSTR /b /c:"%~3" >NUL
        IF ERRORLEVEL 1 (
            SET "stemp=%stemp:~2%"
            echo %stemp%
            IF DEFINED stemp GOTO loop
            SET pos=0
        )
    ) ELSE (
        set len=0
    )
)
(
    endlocal
    set "%~1=%pos%"
    exit /b
)