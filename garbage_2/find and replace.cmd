@echo off
set verfile=url.txt
set tmpfile=MYFILE.tmp
set seek=FA_ALERT
if exist %tmpfile% del /q %tmpfile%
for /f "delims=" %%a in (%verfile%) do (
  (echo %%a)|>nul find /i "%seek%="&&((echo %seek%=0)>>%tmpfile%)
  (echo %%a)|>nul find /i "%seek%="||(echo %%a)>>%tmpfile%
)
copy /y %tmpfile% %verfile% >nul
del /f /q %tmpfile% >nul

pause