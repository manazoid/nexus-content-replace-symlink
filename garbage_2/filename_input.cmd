@ECHO OFF
SETLOCAL

SET /P file=Please enter the thing you want to search in: 

SET logfiles=%~dp0%file%.txt

IF EXIST "%logfiles%" (
    FINDSTR /I /P FA_ALERT "%logfiles%" > "%~dp0%file%_results.log"
) ELSE (
    ECHO No matches for "%logfiles%" found.
    EXIT /B 1
)