@echo off
setlocal disableDelayedExpansion
set "string=;Init., 12, TARO/ BLA BLA CDE"
setlocal enableDelayedExpansion
set ^"string=!string:, TARO/=^

!^"
for /f delims^=^ eol^= %%A in ("!string!") do (
  endlocal
  set "string=%%A"
  goto :break
)
:break
set string
pause