@echo off

rem Open the paths.txt file
rem and read each line
for /f "delims=" %%i in ('type paths.txt') do (

rem Find the index of the "Nexus Content" phrase
set "index=!findstr /r /c:"Nexus Content" %%i!"

rem Extract the substring starting from the index and excluding the last character
rem set "newPath=%%i:~%index%,-1!"
set "newPath=!echo %%i:~%index%,-2!"

rem Write the new path to the newfile.txt file
echo !newPath! >> newfile.txt
)