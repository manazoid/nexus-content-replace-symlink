@echo off

set output=%~dp0output.txt
if exist %output% (
    echo output.txt exists
    set output=%~dp0output.txt
) else (
    echo "THIS WILL START NEW BAT FILE"
    call translate.cmd
    set output=%~dp0output.txt
)

echo %output%