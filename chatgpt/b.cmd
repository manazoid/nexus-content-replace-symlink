@echo off
setlocal enabledelayedexpansion
chcp 65001 > nul

rem Укажите путь к вашему текстовому файлу с данными
set "input_file=input.txt"

rem Очищаем предыдущие массивы (если они были объявлены)
set "prev_name="

rem Счетчик для массивов
set "array_count=0"

rem Читаем содержимое файла построчно
for /f "usebackq delims=" %%i in ("%input_file%") do (
rem Извлекаем имя файла без пути и расширения
for %%j in ("%%i") do set "file_name=%%~nj"

rem Проверяем, если текущее имя файла отличается от предыдущего, то создаем новый массив
if not "!file_name!"=="!prev_name!" (
    if defined prev_name (
        rem Выводим содержимое предыдущего массива
        echo Array !array_count!:
        echo !prev_array!
        echo.
    )
    rem Очищаем предыдущий массив и добавляем текущий путь к нему
    set "prev_array=%%i^"
    set /a "array_count+=1"
) else (
    rem Если текущее имя файла совпадает с предыдущим, добавляем текущий путь к массиву
    set "prev_array=!prev_array! %%i^"
)

rem Обновляем переменную prev_name
set "prev_name=!file_name!"
)

rem Выводим последний массив
if defined prev_array (
echo Array !array_count!:
echo !prev_array:~0,-1!
)

rem Выводим второй элемент первого массива
echo Второй элемент первого массива: !prev_array:~1,1!

endlocal
pause