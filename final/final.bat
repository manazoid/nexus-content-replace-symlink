@echo off
Echo ############################################################
Echo ##                                                        ##
Echo ##                   SymLink Installer                    ##
Echo ##                                                        ##
Echo ##                      !ATTENTION!                       ##
Echo ##                                                        ##
Echo ##   THIS CMD FILE CREATES SYMLINKS AND REPLACE SAMPLES   ##
Echo ##                                                        ##
Echo ############################################################
:: поддержка кириллических символов
chcp 65001 > nul

GOTO isNexusContentNearBy
:: BatchGotAdmin BEGIN https://sites.google.com/site/eneerge/home/BatchGotAdmin | https://ss64.com/nt/rem.html | https://ss64.com/nt/cacls.html
:: Check for permissions
:Permissions
mkdir "%windir%\GotAdminTestCreateDir"
if '%errorlevel%' == '0' (
    rmdir "%windir%\GotAdminTestCreateDir" &goto gotAdmin
) else ( goto UACPrompt )


:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%~dp0getadmin.vbs"
    echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%~dp0getadmin.vbs"

    "%~dp0getadmin.vbs"
    exit /B

:gotAdmin
    if exist "%~dp0getadmin.vbs" ( del "%~dp0getadmin.vbs" )
    pushd "%CD%"
    CD /D "%~dp0"
:: BatchGotAdmin END

:: x86? BEGIN
Set xOS=x64& If "%PROCESSOR_ARCHITECTURE%"=="x86" (
If Not Defined PROCESSOR_ARCHITEW6432 Set xOS=x86
)
Echo OS - %xOS%
If "%xOS%"=="x86" (
  goto x86Windows
) Else (
  goto x64Windows
)
:: x86? END

GOTO START_POINT
:: Source Directories Structure BEGIN

:: Source Directories Structure END

:: CheckCurrentLocation BEGIN

:isNexusContentNearBy
set "dir=%~dp0Nexus Content"
set Log=No & If Exist "%Dir%" FOR /F "usebackq" %%f IN (`Dir "%Dir%\" /b /A:`) DO Set Log=Yes
if "%Log%"=="Yes" (
    GOTO Permissions
) else ( GOTO Fail )

:: CheckCurrentLocation END

:START_POINT

:x64Windows
:: x64 Windows BEGIN

Echo контент здесь
del /F /Q "%~dp0Nexus Content\XPDA\KICK LOUD G.wav" && cmd /c mklink "%~dp0Nexus Content\XPDA\KICK LOUD G.wav" "%~dp0Nexus Content\GVAU\KICK LOUD G.wav"

CD /d "%~dp0"
:: [[[OPTIONAL: IT IS A PLACE FOR ADDITIONAL x64 Windows COMMANDS]]]

:: x64 Windows END
goto OUT

:x86Windows
:: x86 Windows BEGIN

CD /d "%~dp0"
:: [[[OPTIONAL: IT IS A PLACE FOR ADDITIONAL x86 Windows COMMANDS]]]

:: x86 Windows END
goto OUT

:Fail
Echo #####################################################################
Echo ##                                                                 ##
Echo ## !Не найдена директория Nexus Content рядом с местом выполнения, ##
Echo ##           переместите BAT файл рядом с Nexus Content!           ##
Echo ##                                                                 ##
Echo #####################################################################

GOTO OUT

:OUT
PAUSE
EXIT