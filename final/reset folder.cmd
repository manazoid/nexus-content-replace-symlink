@echo off
setlocal
echo RESET FOLDERS

RMDIR /s /q "Nexus Content"
mkdir "Nexus Content"
cd "Nexus Content"
set "file=%~dp0test.txt"
set /A i=0
for /F "usebackq delims=" %%a in ("%file%") do (
    set /A i+=1
    call set array[%%i%%]=%%a
    call set n=%%i%%
)
for /L %%i in (1,1,%n%) do (
    call mkdir %%array[%%i]%% & call copy "%~dp0MutePick.nxs" %%array[%%i]%%\MutePick.nxs>nul
)

pause
